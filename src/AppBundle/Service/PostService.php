<?php
/**
 * @author minhvb
 *
 */

namespace AppBundle\Service;

use AppBundle\Entity\Post;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class PostService {
    private $em;
    private $container;

    public function __construct(EntityManagerInterface $em, ContainerInterface $container)
    {
        $this->em = $em;
        $this->container = $container;
    }

    /**
     * @param Post $post
     * @param User $author
     */
    public function addPost($post, $author)
    {
        /** @var Post $post */
        /** @var UploadedFile $file */
        $file = $post->getImage();

        $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();

        // moves the file to the directory where brochures are stored
        $file->move(
            $this->container->getParameter('upload_directory'),
            $fileName
        );

        $post->setImage($fileName);
        /** set current user as author */
        $post->setAuthor($author);

        $this->em->persist($post);
        $this->em->flush();
    }

    /**
     * @param Post $post
     */
    public function editPost($post)
    {
        /** @var Post $post */
        /** @var UploadedFile $file */
        $file = $post->getImage();

        if ($file instanceof UploadedFile) {
            $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();
            $file->move(
                $this->container->getParameter('upload_directory'),
                $fileName
            );
            $post->setImage($fileName);
        }
    }

    /**
     * @param Post $post
     */
    public function deletePost($post)
    {
        $this->em->remove($post);
        $this->em->flush();
    }

    /**
     * @param User $author
     * @return Post[]|array
     */
    public function getUserPosts($author)
    {
        return $this->em->getRepository(Post::class)->findBy([
            'author' => $author
        ]);
    }

    public function getAllPosts()
    {
        return $this->em->getRepository('AppBundle:Post')->findAll();
    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        return md5(uniqid());
    }
}