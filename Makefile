define read_param
`cat app/config/parameters.yml | grep $1 | awk '{print $$2}' | sed -n '1p'`
endef

define get_env
`readlink  web/.htaccess | sed -r 's/.*htaccess.(.*)/\1/'`
endef

ENV := $(shell $(call get_env))
ifndef ENV
ENV := dev
endif

dev := ffbb.dev7.sutunam.info@ffbb.dev7.sutunam.info

ENV := $(call get_env)
ifndef PHP7.1
ENV := dev
endif

PHP7.1 := $(shell command -v php7.1 2> /dev/null)
ifndef PHP7.1
PHP := php
else
PHP := php7.1
endif
UNAME_S := $(shell uname -s)

NPM := $(shell command npm 2> /dev/null)

# Common jobs
clear_cache:
	$(PHP) bin/console cache:clear --env=$(ENV)

db_update:
	$(PHP) bin/console doctrine:schema:update --force

# Setup
setup: composer_install gen_sonata_media setup_perm htaccess db_create db_update create_admin_user promote_admin_user clear_cache

# Update
update: composer_install db_update clear_cache

db_create:
	$(PHP) bin/console doctrine:database:create --if-not-exists

setup_perm:
ifeq ($(UNAME_S),Linux)
	sudo setfacl -dR -m u:www-data:rwX -m u:$(shell whoami):rwX var
	sudo setfacl -R -m u:www-data:rwX -m u:$(shell whoami):rwX var
else
	@echo "Unknow permissions"
endif

create_admin_user:
	$(PHP) bin/console fos:user:create admin someone@somemail.com 123456

promote_admin_user:
	$(PHP) bin/console fos:user:promote admin ROLE_ADMIN

composer_install:
	$(PHP) $(shell which composer) install

composer_update:
	$(PHP) $(shell which composer) update

assets:
	$(PHP) bin/console assets:install --symlink

htaccess:
	if [ ! -e "web/.htaccess" ]; then ln -s .htaccess.dev web/.htaccess; fi

run_server:
	$(PHP) bin/console server:run

load_fixture:
	$(PHP) bin/console hautelook:fixtures:load --purge-with-truncate

test:
	$(PHP) bin/console doctrine:database:create --if-not-exists --env=test
	$(PHP) bin/console doctrine:schema:update --force --env=test
	$(PHP) bin/console hautelook:fixtures:load --purge-with-truncate --env=test --no-interaction
	@echo "\033[0;33m"PHPSTAN"\033[0m"
	vendor/bin/phpstan analyse src/ tests/ -l 6
	@echo "\n"

	@echo "\033[0;33m"PHPCS"\033[0m"
	vendor/bin/phpcs --standard=PSR2 --colors --ignore="./src/Application/" src/ tests/
	@echo "\n"

	@echo "\033[0;33m"PHPCPD"\033[0m"
	vendor/bin/phpcpd src/ tests/
	@echo "\n"

	@echo "\033[0;33m"TWIG_LINT"\033[0m"
	$(PHP) bin/console lint:twig app/Resources/
	$(PHP) bin/console lint:twig src/
	@echo "\n"

	@echo "\033[0;33m"PHPUNIT"\033[0m"
	vendor/bin/phpunit --configuration phpunit.gitlab-ci.xml
